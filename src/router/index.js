import { createRouter, createWebHistory } from 'vue-router'
import Clients from '@/views/Clients.vue'
import ShowClient from '@/views/ShowClient.vue'
import NewClient from '@/views/NewClient.vue'
import Test from '@/views/Test.vue'

const routes = [
    {
        path: '/',
        redirect: '/clients'
    },
    {
        path: '/clients',
        name: 'Clients',
        component: Clients
    },
    {
        path: '/clients/:id',
        name: 'Show Client',
        component: ShowClient
    },
    {
        path: '/clients/new',
        name: 'New Client',
        component: NewClient
    },
    {
        path: '/test',
        name: 'Test',
        component: Test
    },
]

export { routes };

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router