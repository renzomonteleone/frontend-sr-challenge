import { createPinia } from 'pinia'
import { createApp } from 'vue'
import router from './router';
import App from './App.vue'
import Notifications from 'notiwind'
import './style.css'

const pinia = createPinia()
const app = createApp(App)
    .use(router)
    .use(pinia)
    .use(Notifications)
    
router.isReady().then(() => {
    app.mount('#app');
});
