import axios from "axios";
import { useClientStore } from '@/store/client';
import { useApiStore } from '@/store/api';

const instance = axios.create({
    baseURL: 'https://fe-sr-challenge.baymark.net/api/',
    headers: {
        'Authorization': 'Bearer 8def2320e7486ac76d003497fe22ce5a',
        'Content-Tpye': 'application/json'
    }
});

instance.interceptors.request.use((config) => {
    loading(true)
    return config;
}, (error) => {
    loading(false)
    throwError(error)
    return Promise.reject(error);
});

instance.interceptors.response.use((response) => {
    loading(false)
    return response;
}, (error) => {
    loading(false)
    throwError(error)
    return Promise.reject(error);
});

const loading = (state) => {
    const clientStore = useClientStore()
    clientStore.loading = state;
}

const throwError = (error) => {
    const apiStore = useApiStore()
    apiStore.error(error)
}

export default instance;