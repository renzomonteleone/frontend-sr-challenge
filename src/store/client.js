import axios from "@/http"
import { defineStore } from "pinia"
import { ref } from "vue"
import { useRouter } from "vue-router"
import { notify } from "notiwind"

export const useClientStore = defineStore('client', () => {
    const router = useRouter()
    const clients = ref([])
    const client = ref(null)
    const total = ref(null)
    const links = ref(null)
    const from = ref(0)
    const to = ref(1)
    const currentPage = ref(1)
    const loading = ref(false)

    const fetchPage = async (url) => {
        clients.value = []
        const response = await axios.get(url)
        storeData(response.data)
    }

    const searchByName = async (name = "") => {
        clients.value = []
        const response = await axios.get(`customers?name=${name}`)
        storeData(response.data)
    }

    const getClient = async () => {
        const response = await axios.get(`customers/${router.currentRoute.value.params.id}`)
        client.value = response.data.data
    }

    const updateClient = async (data) => {
        await axios.patch(`customers/${router.currentRoute.value.params.id}`, data)
        notify({
            group: "generic",
            title: "Success",
            text: "Client has been updated"
        }, 4000)
        getClient()
    }

    const newClient = async (data) => {
        await axios.post('customers', data)
        notify({
            group: "generic",
            title: "Success",
            text: "A new client has been created"
        }, 4000)
        router.push("/clients")
    }

    const deleteClient = async () => {
        await axios.delete(`customers/${router.currentRoute.value.params.id}`)
        notify({
            group: "generic",
            title: "Success",
            text: "Client has been deleted"
        }, 4000)
        router.push("/clients")
    }

    const storeData = (data) => {
        clients.value = data.data
        total.value = data.total
        currentPage.value = data.current_page
        links.value = data.links
        from.value = data.from
        to.value = data.to
    }

    return { client, total, from, to, clients, loading, currentPage, links, fetchPage, newClient, searchByName, getClient, updateClient, deleteClient }
})