import { defineStore } from "pinia"
import { notify } from "notiwind"

export const useApiStore = defineStore('api', () => {
    const error = (err) => {
        console.log(err)
        notify({
            group: "error",
            title: "Error",
            text: err.message
        }, 4000)
    }

    return { error }
})