import ClientForm from '@/components/app/client/ClientForm.vue'
import testData from './testData.json'
import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import { createRouter, createWebHistory } from 'vue-router'
import { routes } from "@/router"

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
})

describe('ClientForm.vue', () => {
    it('Fill inputs and check validity', () => {
        const wrapper = mount(ClientForm, {
            global: {
                plugins: [router, createTestingPinia()]
            }
        })

        let inputs = wrapper.findAll('input')
        let selects = wrapper.findAll('select')

        inputs.map((input) => {
            input.element.value = testData[input.element.name]
            expect(input.element.checkValidity()).toBeTruthy()
        })

        selects.map((select) => {
            select.element.value = testData[select.element.name]
            expect(select.element.checkValidity()).toBeTruthy()
        })
    })


    it('Render submit button', () => {
        const wrapper = mount(ClientForm, {
            propsData: {
                client: testData
            },
            global: {
                plugins: [router, createTestingPinia()]
            }
        })

        const button = wrapper.find('button')
        expect(button.exists).toBeTruthy()
    })
})

