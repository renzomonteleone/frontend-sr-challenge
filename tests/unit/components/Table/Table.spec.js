import Table from '@/components/app/clientTable/Table.vue'
import testData from './testData.json'
import { mount, flushPromises } from '@vue/test-utils'
import { setActivePinia, createPinia } from 'pinia'
import { createRouter, createWebHistory } from 'vue-router'
import { routes } from "@/router"

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
})

setActivePinia(createPinia())

describe('Table.vue', () => {
    it('Render q items correctly', () => {
        const wrapper = mount(Table, {
            propsData: {
                rows: testData.data
            },
            global: {
                plugins: [router]
            }
        })
        expect(wrapper.find('tbody').findAll('tr').length).toBe(testData.data.length)
    })

    it('Push client detail on row click', async () => {
        const wrapper = mount(Table, {
            props: {
                rows: testData.data
            },
            global: {
                plugins: [router]
            }
        })
        await wrapper.find('tbody').findAll('tr')[0].trigger('click')
        await router.isReady()
        await flushPromises()
        expect(router.currentRoute.value.fullPath).toBe(`/clients/${wrapper.vm.rows[0].id}`)
    })
})

