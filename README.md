# Frontend Senior Software Engineer UI/UX

## Installation

```
git clone https://gitlab.com/renzomonteleone/frontend-sr-challenge
```

```
cd frontend-sr-challenge
```

```
npm install
```

```
npm run dev
```

## Test

Use vue-test-utils to test

```
npm run test:unit
```
or run specific test
```
npm run test:unit {folder_name}
```

## Dependencies

- pinia
- vue-router
- axios
- lodash.debounce
- notiwind
- jest

